<?php

require ('vendor/autoload.php');

use Recruitment\Webservice\Controller\ProductController;
use Recruitment\Webservice\Service\ProductService;

$service = new ProductService();
$controller = new ProductController($service);

$results = $controller->getAllProducts();

require ('src/view/header.php');

foreach ($results as $result) {
    require ('src/view/row.php');
}

require ('src/view/footer.php');


function h($s) {
    return htmlspecialchars($s, ENT_QUOTES, 'utf-8');
}