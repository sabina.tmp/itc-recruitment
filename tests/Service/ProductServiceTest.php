<?php

namespace Recruitment\Webservice\Tests;

use PHPUnit\Framework\TestCase;
use Recruitment\Webservice\Service\CallApiService;

Class ProductServiceTest extends TestCase
{
    /**
     * @var CallApiService
     */
    protected $service;

    public function setUp()
    {
        $this->service = new CallApiService;
    }

    protected function tearDown()
    {
        unset($this->service);
    }
   
   
    public function testGetListSuccessful()
    {
        $service = $this->createMock(CallApiService::class, array('callApi') );

        $url = 'http://domain.com';

        $service->expects($this->once())
            ->method('callApi')
            ->with($this->equalTo($url))
            ->willReturn(json_decode('{"products": {"id1": "name1", "id2": "name2"}}'));

        $results = $service->callApi($url);

        $this->assertInternalType('object', $results);

        $this->assertObjectHasAttribute('products', $results);
        $this->assertInternalType('object', $results->products);
        
        $this->assertObjectHasAttribute('id1', $results->products);
        $this->assertInternalType('string', $results->products->id1);
    }

}