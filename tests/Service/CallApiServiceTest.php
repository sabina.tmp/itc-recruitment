<?php

namespace Recruitment\Webservice\Tests;

use PHPUnit\Framework\TestCase;
use Recruitment\Webservice\Service\CallApiService;

Class CallApiTest extends TestCase
{
    /**
     * @var CallApiService
     */
    protected $service;

    public function setUp()
    {
        $this->service = new CallApiService;
    }


    protected function tearDown()
    {
        unset($this->service);
    }
   

    public function testCallAPIWithoutParamUnsuccessful()
    {
        $this->expectException(\TypeError::class);

        $this->service->callApi();
    }

    
    public function testCallAPIWithWrongParamUnsuccessful()
    {
        $this->expectException(\PHPUnit\Framework\Error\Warning::class);

        $this->service->callApi('not-a-url');
    }

}