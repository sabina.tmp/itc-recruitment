<tr>
    <td><?= h($result->name ?? ''); ?></td>
    <td><?= h($result->description ?? ''); ?></td>
    <td><?= h($result->type ?? ''); ?></td>
    <td>
        <ul>
            <?php foreach($result->suppliers ?? [] as $supplier) { ?>
                <li><?= h($supplier); ?></li>
            <?php } ?>
        </ul>    
    </td>
</tr>