<?php

namespace Recruitment\Webservice\Controller; // naming things is hard :(

use Recruitment\Webservice\Service\ProductService;

Class ProductController
{
    private $service;

    /**
     * Create the ProductController class
     *
     * @param ProductService $service
     * @return void
     */
    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }

    /**
     * Get list of all products and the details about them
     *
     * @return array
     */
    public function getAllProducts(): array
    {
        $productList = $this->service->getList();

        $results = [];
        foreach($productList->products as $id => $name) {
            $results[$id] = $this->service->getItem($id)->{$id};
        }

        return $results;
    }

}