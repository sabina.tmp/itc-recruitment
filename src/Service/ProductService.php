<?php

namespace Recruitment\Webservice\Service;


Class ProductService extends CallApiService
{
    /**
     * @var string baseUrl - todo: should be moved to config
     */
    private $baseUrl = 'https://www.itccompliance.co.uk/recruitment-webservice/api/';

    /**
     * getList
     *
     * @return stdClass
     */
    public function getList(): \stdClass
    {
        $url = $this->baseUrl . 'list';

        return $this->callApi($url);
    }
    
    /**
     * getItem
     *
     * @param  mixed $id
     * @return stdClass
     */
    public function getItem(string $id): \stdClass
    {
        $url = $this->baseUrl . 'info?id=' . $id;

        return $this->callApi($url);
    }
}
