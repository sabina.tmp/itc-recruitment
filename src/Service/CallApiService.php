<?php

namespace Recruitment\Webservice\Service;


Class CallApiService
{
    /**
     *
     * @param [string] $url
     * 
     * @return stdClass
     */
    public function callApi(string $url): \stdClass
    {
        do {
            try {
                $result = file_get_contents($url);
                $result = json_decode($result);
            } catch (\Exception $e) { 
                // todo: log

                //throw error forward. doesn't maker sense to try again if the url can't be opened
                throw $e;
            }
        } while (!$result || !empty($result->error));

        return $result;
    }
}
